# Command to install dependencies
npm install

# Command to start server
npm start

# API URL
http://localhost:4000/

# Add a .env file consisting of
MAIL = youremail


PWD  = yourpassword
